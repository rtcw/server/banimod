FROM msh100/rtcw

ADD banimod.tar.gz /home/game/

COPY --chown=game:game wolfded.x86 /home/game/wolfded.x86
COPY --chown=game:game server.cfg /home/game/main/server.cfg.tpl

RUN chmod +x /home/game/wolfded.x86

EXPOSE 27960/udp

ENTRYPOINT ["/home/game/start"]